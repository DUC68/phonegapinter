$( document ).ready(function() {

    $('#save').click(function() {
        saveSettings();
    });

    function loadSettings() {
        $('#myname').text(localStorage.name);
    }

    function saveSettings() {
        var name = document.getElementById("name").value;
        localStorage.name = name;
    }

	if(navigator.onLine){
        updateOnlineStatus();
    } else {
        updateOfflineStatus(); 
    }

    function updateOnlineStatus(event) {
        $( "body" ).addClass("online");
        $( "body" ).removeClass("offline");
    }
    
    function updateOfflineStatus(event) {
        $( "body" ).addClass("offline");
        $( "body" ).removeClass("online");
    }
    
    window.addEventListener('online',  updateOnlineStatus);
    window.addEventListener('offline', updateOfflineStatus);

    loadSettings();
});